#Simple script to initialize the DB

require "sqlite3"

db = SQLite3::Database.new "./db/botwit.db"

#Interaction table will save the pre-defined interactions (commands)
db.execute <<-SQL
  CREATE TABLE interaction (
    id INTEGER PRIMARY KEY,
    text TEXT,
    accessLvlNeeded  INTEGER
  );
SQL

#One interactioncan have multiple answers, and one answer can be provided with different interactions
db.execute <<-SQL
  CREATE TABLE interaction_answer (
    interactionId INTEGER,
    answerId INTEGER,
    FOREIGN KEY(interactionId) REFERENCES interaction(id),
    FOREIGN KEY(answerId) REFERENCES answer(id)
  );
SQL

#Answer table will keep the answer/answers for each interaction
db.execute <<-SQL
CREATE TABLE answer (
    id INTEGER PRIMARY KEY,
    text TEXT
  );
SQL

#User table will keep users and it's privileges (Only users with special privileges will appear)
db.execute <<-SQL
CREATE TABLE user (
    id INTEGER PRIMARY KEY,
    name VARCHAR,
    accessLevel  INTEGER
  );
SQL

#We load simple interactions
db.execute <<-SQL
  INSERT INTO interaction(text, accessLvlNeeded) VALUES("hello",0),("hi",0),("how are you",0),("hola",0),("!pillow",0),("hour",0),("what hour",0),("tell me a joke",0),("joke",0),("!weather",0),("!news",0),("!ban",1),("!op",1),("!normaluser",1),("!addcommand",1),("!changepermission",1),("!salute",0);
SQL

db.execute <<-SQL
  INSERT INTO answer(text) VALUES("Hello! %user. How are you?"),("Hey %user! Doing fine?"),("%user throws a pillow at %receiver."),("Hi %user, it's %hour."),("%user No."),("%user %weather"),("%user %news"),(1),("%user says hi to %receiver");
SQL

db.execute <<-SQL
  INSERT INTO interaction_answer(interactionId, answerId) VALUES(1,1),(1,2),(2,1),(2,2),(3,1),(3,2),(4,1),(4,2),(5,3),(6,4),(7,4),(8,5),(9,5),(10,6),(11,7),(12,8),(13,8),(14,8),(15,8),(16,8),(17,9);
SQL

db.execute <<-SQL
  INSERT INTO user(name,accessLevel) VALUES("user",1);
SQL
