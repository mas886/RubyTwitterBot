require './GetConfig'
require './generator/ProcessAnswer'

class Bot

  def loopMain(client)
    #Set twit count to 0
    c=0
    #This will reply to the twits since the bot was started
    lastId=client.mentions_timeline(count: 1)[0].id

    #Main loop for the bot
    while true
      tweets = client.mentions_timeline(since_id: lastId)#count: 5

      if tweets.length > 0
        tweets.each { |tweet|
          c=c+1
          puts "\nTwit number #{c} with id #{tweet.id} from #{tweet.user.screen_name}"
          puts tweet.full_text
          #Answering the tweet.
          res=ProcessAnswer.new;
          res.processTweet(tweet, client)
          lastId=tweet.id
        }
      else
        puts "No new twits!"
      end
      #We wait 60 seconds to prevent spam
      sleep 60
    end

  rescue Twitter::Error::TooManyRequests => error
    puts "\n-Too many requests error.\n"
    # NOTE: Your process could go to sleep for up to 15 minutes but if you
    # retry any sooner, it will almost certainly fail with the same exception.
    sleep error.rate_limit.reset_in + 1
    retry

  rescue Twitter::Error => error
    # Saves timeouts and no internet errors (And mostly any error I might have involving Twitter class)
    puts error
    sleep 120
    retry
  end

end
