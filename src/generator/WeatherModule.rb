require 'json'
require 'net/http'

#this module works with openweathermap.org API

class WeatherModule

  def initialize()
      @apiKey = ""
  end

  def getAnswer(city)
    cityId=getCityId(city)
    if cityId!="City Not Found"
      weatherArray=getApiAnswer(cityId)
      return generateString(weatherArray, city)
    else
      return "We don't have that city"
    end
  end

  def getCityId(name)
    file = File.read('./db/city.list.json')
    cityDb = JSON.parse(file)
    cityDb.map { |x| if x['name'].downcase==name
      return x['id'] end}
    return "City Not Found"
  end

  def getApiAnswer(cityId)
    url = URI.parse("http://api.openweathermap.org/data/2.5/forecast?id=#{cityId}&units=metric&APPID=#{@apiKey}")
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|
      http.request(req)
    }
    result = JSON.parse(res.body)
    weather= result['list'].first
    return weather
  end

  def generateString(weatherArray, cityName)
    weatherString="Weather on #{cityName.capitalize}: Temperature #{weatherArray['main']['temp']}, Humidity #{weatherArray['main']['humidity']}%, Wind Speed #{weatherArray['wind']['speed']}, #{weatherArray['weather'].first['description']}"
    return weatherString
  end

end
