require './generator/Response'

class ProcessAnswer

  def processTweet(tweet, client)
    interaction = tweet.full_text.gsub("@TheBotwit", "").strip.downcase
    res=Response.new;
    answer=res.getAnswer(interaction,tweet.user.screen_name)
    puts "Response: #{answer}"
    if answer!="Bad syntax" && answer!="No answers" && answer!="Access Not Allowed"
      client.update(answer, in_reply_to_status:tweet)
    end
  end

end
