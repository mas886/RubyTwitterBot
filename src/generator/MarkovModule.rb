class MarkovModule

  $stopWord="\n"
  $markoBrainFile="./db/markov_brain.txt"

  def getMarkovAnswer(msg)
    $markov=add_to_brain(msg)
    return generate_sentence(msg)
  end

  def add_to_brain(msg, writeToFile=true, chain_length=3,markov=$markov)
    if writeToFile
      File.open($markoBrainFile, 'a') { |file| file.write(msg+"\n") }
    end
    buf=[]
    buf=[$stopWord]*chain_length
    msg.split.each do |chr|
      if !buf.include? "\n"
        if markov[buf.dup].class.to_s!="Array"
          markov[buf.dup]=[]
        end
        markov[buf.dup] << chr
        p "Added: "+buf.dup.to_s+": "+markov[buf.dup].to_s
        if !markov[buf.dup].include? "\n"
          markov[buf.dup] << $stopWord
        end
      end
      buf.delete_at(0)
      buf << chr
    end
    return markov;
  end

  def generate_sentence(msg, chain_length=3, max_words=10000, markov=$markov)
    buf = msg.split.take chain_length
    if (msg.split.length>chain_length)
      message=buf.dup
    else
      message=[]
      for i in 0..chain_length-1
        word=markov.to_a.sample(1).sample.sample
        if word.class.to_s=="Array"
          message << word.sample
        else
          message << word
        end
      end
    end
    for i in 0..max_words-1
      begin
        next_word= markov[buf].sample
        if next_word==$stopWord
          break
        end
        message << next_word
        buf.delete_at(0)
        buf << next_word
      rescue NoMethodError
        if next_word==0
          break
        end
      end
    end
    return message.join(" ")
  end

  def initializeMarkovBrain(file=$markoBrainFile)
    textBrain = File.read(file).split("\n")
    $markov = Hash.new {0}
    textBrain.each do |sentence|
      $markov=add_to_brain(sentence, writeToFile=false)
    end
  end
end
