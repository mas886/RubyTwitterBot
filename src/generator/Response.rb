require "sqlite3"
require "./classes/interaction"
require "./classes/user"
require "./generator/MarkovModule.rb"
require "./generator/WeatherModule.rb"
require "./generator/RSSModule.rb"
require "./classes/AdminModule.rb"

class Response

  def getAnswer(interaction,sender)
    if checkUserAccessLever(sender)
      if (interaction[0]=='!')
        #We will come here if the command starts with "!" (directed to another user)
        res = answerCommand(interaction, sender)
        print "Command check response was: "+res+"\n"
      else
        #Here if it's a normal command (directed to the sender)
        res= checkDb(interaction,sender)
      end
      if res=="1"
        triggerAdminCommand(interaction)
        print "Performing admin command by: "+sender+"\n"
        return "No answers"
      end
      #We replace most of the keywords. return
      res=replaceKeywords(res)
      res=res.gsub("%user", "@#{sender}")
      if res=="No answers"
        asd=MarkovModule.new
        res=asd.getMarkovAnswer(interaction)
        res="@#{sender} "+res
      end
      if res==1
          res="No answers"
      end
      return res
    else
      return "Access Not Allowed"
    end
  end

  def answerCommand(interaction, sender)
    if (interaction.split(' ').length>=2)
      if checkInteractionAccess(interaction.split[0], sender)
        answer=checkDb(interaction.split[0])
        #If the answer contains any %receiver the substring %receiver will be replaced by the target user (That it's the last strig of the command).
        answer=answer.gsub("%receiver", interaction.split(' ')[interaction.split(' ').length-1])
        answer=answer.gsub("%weather", weatherAnswer(interaction.split(' ').last(interaction.split.length-1).join(" ")))
        answer=answer.gsub("%news", rssAnswer(interaction.split(' ').last(interaction.split.length-1).join(" ")))
        return answer
      else
        return "Access Not Allowed"
      end
    end
    return "Bad syntax"
  end

  def weatherAnswer(city)
    weather=WeatherModule.new
    return weather.getAnswer(city)
  end

  def rssAnswer(topic)
    rss = RSSModule.new
    return rss.getAnswer(topic)
  end

  def checkDb(interaction,sender=1111)
    db = SQLite3::Database.new "./db/botwit.db"
    query = db.prepare('SELECT answer.text FROM answer JOIN interaction_answer,interaction WHERE interaction.id=interaction_answer.interactionId AND answer.id=interaction_answer.answerId AND interaction.text= :interaction')
    responses=[]
    query.execute(":interaction"=>interaction) do |ans|
      ans.each {|a| responses << a[0]}
    end
    if responses.length>0
      if sender!=1111
        if !checkInteractionAccess(interaction, sender)
          return "Access Not Allowed"
        end
      end
      return responses[rand(responses.length)]
    end
    return "No answers"
  end

  def replaceKeywords(message)
    time = Time.new
    message=message.gsub("%hour", time.strftime("%Y-%m-%d %H:%M:%S"))
    return message
  end

  def checkUserAccessLever(sender)
    #This will be used to ensure an user is not banned.
    userAccessLevel=User.new.getUserAccessLevel(sender)
    if userAccessLevel<0
      return false
    else
      return true
    end
  end

  def triggerAdminCommand(command)
    admin=AdminModule.new
    return admin.performAction(command)
  end

  def checkInteractionAccess(interaction,sender)
    interAccessLevel=Interaction.new.getInteractionAccessLevel(interaction)
    userAccessLevel=User.new.getUserAccessLevel(sender)
    if userAccessLevel >= interAccessLevel
      return true
    else
      return false
    end
  end

end
