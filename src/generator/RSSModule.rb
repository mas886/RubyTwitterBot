require 'rss'
require 'open-uri'
require 'openssl'

class RSSModule

  def initialize()
      @urlShortenerAPIkey=""
  end

  def getAnswer(about)
    topic=getSource(about)
    if topic!="Topic not found"
      return getRSSEntries(topic)
    else
      return topic
    end
  end

  def getSource(about)
      file = File.read('./db/RSSSources.json')
      cityDb = JSON.parse(file).map do |source|
        source.map { |x| if x[0]==about
                      return x[1].sample end}
      end
      return "Topic not found"
  end

  def getRSSEntries(topic)
    rss_feed = topic
    rss_content = ""
    open(rss_feed) do |f|
        rss_content = f.read
    end
    rss = RSS::Parser.parse(rss_content, false)
    content=""
    rss.items.sample(2).each do |item|
      #puts "#{item.title} "
      if item.title.length >25
        content+=item.title[0..22]+"..."
      else
        content+=item.title
      end
      content+=" Link: #{shortenLink(item.link)}\n"
    end
    return content
  end

  def shortenLink(link)
    #This will work with google's shortener API
    uri = URI.parse("https://www.googleapis.com")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    request = Net::HTTP::Post.new("/urlshortener/v1/url?key="+@urlShortenerAPIkey)
    request.add_field('Content-Type', 'application/json')
    request.body = '{"longUrl": "'+link+'"}'
    response = http.request(request)
    apiAns = JSON.parse(response.body)
    return apiAns['id']
  end

end
