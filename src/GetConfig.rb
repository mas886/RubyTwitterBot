require 'twitter'

class Config
  $file='Config'

  def getConfig
    contents = File.read($file).split("\n")
    
    #We check if there's information on the Config file
    correctFormat=true
    
    for value in contents do
      
      if !correctFormat
        break
      end
      
      if value.split('=').length <=1
        correctFormat=false
        puts "Please add access tokens on Config file."
      elsif (value.split('=')[1].length < 5)
        correctFormat=false
        puts "Please add access tokens on Config file."
      end
      
    end
    
    if correctFormat
      
      client = Twitter::REST::Client.new do |config|
        config.consumer_key = contents[0].split('=')[1]
        config.consumer_secret = contents[1].split('=')[1]
        config.access_token = contents[2].split('=')[1]
        config.access_token_secret = contents[3].split('=')[1]
        
      end
      return client
    else
      #If connection was not possible a null value is returned
      return nil
    end
  end

end