require 'twitter'
require './Bot'
require "./generator/MarkovModule.rb"

class Main
  #We get information from the Config file
  config=Config.new
  client = config.getConfig
  if client!=nil
    #Awake message (By now disabled to avoid SPAM)
    #client.update("Hey there it's #{time.inspect} and I'm awake!")
    time = Time.new
    puts ("\n------------------The bot is awake!--------------------\n\n")
    puts "-Initializing markov brain-\n"
    asd=MarkovModule.new
    asd.initializeMarkovBrain
    loop=Bot.new
    loop.loopMain(client)
  else
    puts "The bot could not start, ending."
  end

end
