require "./classes/user.rb"
require "./classes/interaction.rb"

class AdminModule

  def performAction(command)
    commandSplitted=command.split
    if commandSplitted[0]=="!ban"
      banUser(commandSplitted)
    elsif commandSplitted[0]=="!op"
      opUser(commandSplitted)
    elsif commandSplitted[0]=="!normaluser"
      normalizeUser(commandSplitted)
    elsif commandSplitted[0]=="!addcommand"
      addInteraction(commandSplitted)
    elsif commandSplitted[0]=="!changepermission"
      changePermission(commandSplitted)
    end
  end

  def changePermission(commandSplitted)
    inter=Interaction.new
    permission = Integer(commandSplitted[commandSplitted.length-1]) rescue nil
    if commandSplitted.length >= 3 && permission!=nil
      #print "interaction: "+commandSplitted[1..commandSplitted.length-2].to_s
      inter.changePermission(commandSplitted[1..commandSplitted.length-2].join(" "), permission)
    end
  end

  def addInteraction(commandSplitted)
    #to add an interaction use the following syntax !addCommand /interaction trigger /answer to be given
    #note the importance of the slash, it's used as a separator so the inside text can contain spaces
    interaction=Interaction.new
    commandSplitted=commandSplitted.join(" ").split("/")
    if commandSplitted.length ==3
      interaction.addInteraction(commandSplitted[1].strip,commandSplitted[2].strip)
    end
  end

  def banUser(commandSplitted)
    user=User.new
    user.setAccessLevel(commandSplitted[1],-1)
  end

  def opUser(commandSplitted)
    user=User.new
    user.setAccessLevel(commandSplitted[1],1)
  end

  def normalizeUser(commandSplitted)
    user=User.new
    user.setAccessLevel(commandSplitted[1],0)
  end

end
