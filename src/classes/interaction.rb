#This class will handle interactions between the program and interaction table in the db
require "sqlite3"

class Interaction

  def getInteractionAccessLevel(interaction)
    db = SQLite3::Database.new "./db/botwit.db"
    query = db.prepare('SELECT accessLvlNeeded FROM interaction WHERE text= :interaction')
    responses=[]
    query.execute(":interaction"=>interaction) do |ans|
      ans.each {|a| responses << a[0]}
    end
    if responses.length > 0
      return responses[0]
    else
      return -1
    end
  end

  def changePermission(interaction, newPermissionLevel)
    if checkIfInteractionExist(interaction)
      db = SQLite3::Database.new "./db/botwit.db"
      query = db.prepare('UPDATE interaction SET accessLvlNeeded = :newPermissionLevel WHERE text=:interaction')
      responses=[]
      query.execute(":interaction"=>interaction, ":newPermissionLevel"=>newPermissionLevel)
    end
  end

  def addInteraction(trigger,answer)
    addInteractionIfNotExist(trigger)
    addAnswerIfNotExist(answer)
    makeRelation(trigger,answer)
  end

  def makeRelation(trigger,answer)
    db = SQLite3::Database.new "./db/botwit.db"
    query = db.prepare('INSERT INTO interaction_answer (interactionId,answerId) VALUES((SELECT id FROM interaction WHERE text = :interaction),(SELECT id FROM answer WHERE text = :answer))')
    query.execute(":interaction"=>trigger,":answer"=>answer)
  end

  def addInteractionIfNotExist(trigger)
    if !checkIfInteractionExist(trigger)
        db = SQLite3::Database.new "./db/botwit.db"
        query = db.prepare('INSERT INTO interaction(text,accessLvlNeeded) VALUES(:text,0)')
        query.execute(":text"=>trigger)
    end
  end

  def addAnswerIfNotExist(answer)
    if !checkIfAnswerExist(answer)
        db = SQLite3::Database.new "./db/botwit.db"
        query = db.prepare('INSERT INTO answer(text) VALUES(:text)')
        query.execute(":text"=>answer)
    end
  end

  def checkIfInteractionExist(trigger)
      db = SQLite3::Database.new "./db/botwit.db"
      query = db.prepare('SELECT accessLvlNeeded FROM interaction WHERE text= :text')
      responses=[]
      query.execute(":text"=>trigger) do |ans|
        ans.each {|a| responses << a[0]}
      end
      if responses.length>0
        return true
      else
        return false
      end
  end

  def checkIfAnswerExist(answer)
      db = SQLite3::Database.new "./db/botwit.db"
      query = db.prepare('SELECT id FROM answer WHERE text= :text')
      responses=[]
      query.execute(":text"=>answer) do |ans|
        ans.each {|a| responses << a[0]}
      end
      if responses.length>0
        return true
      else
        return false
      end
  end

end
