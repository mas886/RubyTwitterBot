#This class will handle interactions between the program and users table in the db
require "sqlite3"

class User

  def getUserAccessLevel(user)
    db = SQLite3::Database.new "./db/botwit.db"
    query = db.prepare('SELECT accessLevel FROM user WHERE name= :user')
    responses=[]
    query.execute(":user"=>user) do |ans|
      ans.each {|a| responses << a[0]}
    end
    if responses.length>0
      return responses[0]
    else
      return 0
    end
  end

  def setAccessLevel(user,accessLevel)
    #puts "User: "+user+" Access Level: "+accessLevel.to_s+"\n"
    addUserIfNotSet(user)
    setUserAccessLevel(user,accessLevel)
  end

  def setUserAccessLevel(user,accessLevel)
    db = SQLite3::Database.new "./db/botwit.db"
    query = db.prepare('UPDATE user SET accessLevel = :accessLevel WHERE name = :user')
    responses=[]
    query.execute(":user"=>user,":accessLevel"=>accessLevel)
  end

  def addUserIfNotSet(user)
    if !checkIsUserSet(user)
      db = SQLite3::Database.new "./db/botwit.db"
      query = db.prepare('INSERT INTO user(name, accessLevel) VALUES (:user,0)')
      responses=[]
      query.execute(":user"=>user)
    end
  end

  def checkIsUserSet(user)
      db = SQLite3::Database.new "./db/botwit.db"
      query = db.prepare('SELECT accessLevel FROM user WHERE name= :user')
      responses=[]
      query.execute(":user"=>user) do |ans|
        ans.each {|a| responses << a[0]}
      end
      if responses.length>0
        return true
      else
        return false
      end
  end

end
